FROM ubuntu:20.04
LABEL maintainer="Robert Sturla"

ENV PACKER_VERSION=1.7.5
ENV DEBIAN_FRONTEND="noninteractive" TZ="Europe/London"

RUN apt-get update
RUN apt-get install -y git bash wget unzip openssl software-properties-common xorriso
RUN add-apt-repository --yes --update ppa:ansible/ansible
RUN apt-get install -y ansible

RUN useradd -ms /bin/bash packer
RUN groupadd runner
RUN usermod -aG runner packer

ADD https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip ./

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

RUN unzip packer_${PACKER_VERSION}_linux_amd64.zip -d /bin
RUN rm -f packer_${PACKER_VERSION}_linux_amd64.zip

USER packer
ENTRYPOINT ["/bin/packer"]
